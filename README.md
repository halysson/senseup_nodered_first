<h1>How to run this project:</h1>

<p>1 - git clone https://gitlab.com/halyssonfreitassenseup/senseup_nodered_first.git</p>
<p>2 - cd senseup_nodered_first/</p>
<p>3 - cd node_red_data/</p>
<p>4 - npm install</p>
<p>5 - cd ..</p>
<p>6 - docker-compose up</p>
<p>7 - Open the PHPAdmin in a browser: http://localhost:8080/</p>
<p>8 - The credentials are:</p>
<pre>
        -   System	    MySQL
        -   Server	    mysql_db
        -   Username    root
        -   Password	root
        -   Database    senseup</pre>
<p>9 - In IMPORT feature select inner the project root folde: my_sql/users.sql</p>
<p>10 - Now, all in done. Access in a browser http://192.168.100.15:1880/ui/#!/1?socketid=vl8ElTKg2w22dT-0AAAB and click in button refresh to see the data from database.</p>












